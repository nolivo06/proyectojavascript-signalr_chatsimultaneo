﻿
using Microsoft.AspNetCore.SignalR;

namespace ProyectoChatConSignalR.Servicio
{
    public class ChatHub:Hub
    {
        public async Task Send(string name,string message)
        {
           await Clients.All.SendAsync("ReceiveMessage",name, message);

        }
    }
}
