﻿
"use strict"

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

connection.on("ReceiveMessage", function (user, message) {
    var msg = sanitizeMessage(message);
    var encodeMsg = `${user}: ${msg}`;
    var li = document.createElement("li");
    li.textContent = encodeMsg;
    document.getElementById("messageList").appendChild(li);
});

connection.start().then(function () {
    document.getElementById("sendButtom").disable = false;
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButtom").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;

    if (user && message) {
        connection.invoke("Send", user, message).catch(function (err) {
            return console.error(err.toString());
        });

        document.getElementById("messageInput").value = "";
    }
    
    event.preventDefault();
});

function sanitizeMessage(message) {
    return message.replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;");
}